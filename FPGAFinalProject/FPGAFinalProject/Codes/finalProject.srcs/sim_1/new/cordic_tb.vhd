library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity cordic_tb is
    generic(n : integer := 18; 
                  m: integer := 18;
                  LogN : integer := 5);
end;

architecture bench of cordic_tb is

  component cordic
      generic(n : integer; 
              m: integer;
              LogN : integer);
      port(clk, reset: in std_logic;
           beta : in std_logic_vector(n downto 0);
           start : in std_logic;
           ready : out std_logic;
           result_ready : out std_logic;
           sinBeta : out std_logic_vector(n downto 0));
  end component;

  signal clk, reset: std_logic;
  signal beta: std_logic_vector(n downto 0);
  signal start: std_logic;
  signal ready: std_logic;
  signal result_ready: std_logic;
  signal sinBeta: std_logic_vector(n downto 0);

  constant clock_period: time := 5 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: cordic generic map ( n            => n,
                            m            => m,
                            LogN         =>  LogN)
                 port map ( clk          => clk,
                            reset        => reset,
                            beta         => beta,
                            start        => start,
                            ready        => ready,
                            result_ready => result_ready,
                            sinBeta      => sinBeta );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset <= '1';
    wait for clock_period/2;
   ------------------------------------ 
    reset <= '0';
    
--0
    start <= '0';
    wait for clock_period;
    start <= '1';
    beta <= "1010011011100100101";
    wait for clock_period;
 
--1
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0001100101011011101";
    wait for clock_period;

--2    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0010010000001011110";
    wait for clock_period;
    
--3
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0000010100000100010";
    wait for clock_period;
    
--4
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "1010011100110111011";
    wait for clock_period;
    
--5
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0010110011101101110";
    wait for clock_period;
    
--6
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0010110010010011010";
    wait for clock_period;

--7
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "1000001011001000000";
    wait for clock_period;

--8
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0001110110001001101";
    wait for clock_period;

--9
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0010110101101011000";
    wait for clock_period;

--10    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "1010110110010100010";
    wait for clock_period;

--11    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0000111011101101110";
    wait for clock_period;

--12    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "1010101001111011111";
    wait for clock_period;

--13    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0000111110011111001";
    wait for clock_period;

--14    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "1001111000110101101";
    wait for clock_period;

--15    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "1000111000110110001";
    wait for clock_period;

--16    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0010010000001010001";
    wait for clock_period;

--17    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "1001001011110100101";
    wait for clock_period;

--18    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "1001000100010111100";
    wait for clock_period;

--19    
    start <= '0';
    wait for clock_period*3;
    start <= '1';
    beta <= "0001101000100100110";
    wait for clock_period;
   
    start <= '0';
    wait for 200 * clock_period;

    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;