---------------------------------------------------------------------------
-- CORDIC COMPONENT --
-- FPGA PROJECT --
-- 9423087 Shamisa Kaspour --
-- 9423115 Mohammad Mehdi Naseri --
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity CordicComp is
    generic(
        N    : integer := 18;
        EBIT : integer := 5; --[log2(N)]
        I    : integer := 0  --number of iteration
        );
    port(
        clk, reset       : in std_logic;
        start            : in std_logic;
        sigmaOld         : in std_logic;
        tanInverse       : in std_logic_vector(N + EBIT downto 0);
        zOld, xOld, yOld : in std_logic_vector(N + EBIT downto 0);
        done             : out std_logic;
        sigmaNew         : out std_logic;
        zNew, xNew, yNew : out std_logic_vector(N + EBIT downto 0)
        );
end CordicComp;

architecture Behavioral of CordicComp is
    type state_type is (idle, busy, donee);
    signal stateReg            : state_Type;
    signal stateNext           : state_Type;
    signal startReg            : std_logic;
    signal edge                : std_logic;
    signal signOfY2        : std_logic;
    signal signOfX2        : std_logic;
    signal tInverseSign        : std_logic;
    signal maxXo, maxYo, maxZo : std_logic;
    signal x1x2XOR             : std_logic;
    signal y1y2XOR             : std_logic;
    signal z1z2XOR             : std_logic;
    signal sigmaReg, sigmaNext : std_logic;
    signal xN_reg, yN_reg, zN_reg, xN_next, yN_next, zN_next : unsigned(N + EBIT downto 0);
    signal xOReg, yOReg        : unsigned(N + EBIT downto 0);
    signal xONext, yONext      : unsigned(N + EBIT downto 0);
    signal zOReg, zONext       : unsigned(N + EBIT downto 0);
    signal tanIReg             : unsigned(N + EBIT downto 0);
    signal xShifted, yShifted  : unsigned(N + EBIT downto 0);

begin
    tanIReg <= unsigned(tanInverse);
    
    --state and data register
    process(clk, reset)
    begin
        if(reset = '1') then
            stateReg <= idle;
            xOReg    <= (others => '0');
            yOReg    <= (others => '0');
            zOReg    <= (others => '0');
            
            xN_reg <= (others => '0');
            yN_reg <= (others => '0');
            zN_reg <= (others => '0');
            sigmaReg <= '0';
            
        elsif (clk'event and clk = '1') then
            startReg <= start;
            stateReg <= stateNext;
            sigmaReg <= sigmaNext;
            xOReg    <= xONext;
            yOReg    <= yONext;
            zOReg    <= zONext;
            
            xN_reg <= xN_next;
            yN_reg <= yN_next;
            zN_reg <= zN_next;
            
        end if;
    end process;
    
    --start edge detection
    edge <= (not startReg) and start;
    
    process(edge, stateReg, sigmaReg, xOReg, yOReg, zOReg, x1x2XOR, y1y2XOR, z1z2XOR, 
        xShifted, yShifted, tanIReg, maxXo, maxYo, maxZo, signOfX2, signOfY2, tInverseSign)
    begin      
        done      <= '0';
        stateNext <= stateReg;
        sigmaNext <= sigmaReg;
        xONext    <= xOReg;
        yONext    <= yOReg;
        zONext    <= zOReg;
        
        xN_next <= xN_reg;
        yN_next <= yN_reg;
        zN_next <= zN_reg;

        case stateReg is
            when idle  =>
                if (edge = '1') then
                    xONext    <= unsigned(xOld);
                    yONext    <= unsigned(yOld);
                    zONext    <= unsigned(zOld);
                    sigmaNext <= sigmaOld;
                    stateNext <= busy;
                end if;
            when busy  =>
                if (x1x2XOR = '1') then
                    if (maxXo = '1') then
                        xN_next(N + EBIT) <= xOReg(N + EBIT);
                        xN_next(N + EBIT - 1 downto 0) <= xOReg(N + EBIT - 1 downto 0) - yShifted(N + EBIT - 1 downto 0); 
                    else
                        xN_next(N + EBIT) <= signOfX2;
                        xN_next(N + EBIT - 1 downto 0) <= yShifted(N + EBIT - 1 downto 0) - xOReg(N + EBIT - 1 downto 0); 
                    end if;
                else
                    xN_next(N + EBIT - 1 downto 0) <= xOReg(N + EBIT - 1 downto 0) + yShifted(N + EBIT - 1 downto 0); 
                    xN_next(N + EBIT) <= xOReg(N + EBIT);
                end if;
                
                if (y1y2XOR = '1') then
                    if (maxYo = '1') then
                        yN_next(N + EBIT) <= yOReg(N + EBIT);
                        yN_next(N + EBIT - 1 downto 0) <= yOReg(N + EBIT - 1 downto 0) - xShifted(N + EBIT - 1 downto 0); 
                    else
                        yN_next(N + EBIT) <= signOfY2;
                        yN_next(N + EBIT - 1 downto 0) <= xShifted(N + EBIT - 1 downto 0) - yOReg(N + EBIT - 1 downto 0); 
                    end if;
                else
                    yN_next(N + EBIT - 1 downto 0) <= xShifted(N + EBIT - 1 downto 0) + yOReg(N + EBIT - 1 downto 0); 
                    yN_next(N + EBIT) <= yOReg(N + EBIT);
                 end if;
                 
                if (z1z2XOR = '1') then
                    if (maxZo = '1') then
                        zN_next(N + EBIT) <= zOReg(N + EBIT);
                        zN_next(N + EBIT - 1 downto 0) <= zOReg(N + EBIT - 1 downto 0) - tanIReg(N + EBIT - 1 downto 0); 
                    else
                       zN_next(N + EBIT) <= tInverseSign;
                       zN_next(N + EBIT - 1 downto 0) <= tanIReg(N + EBIT - 1 downto 0) - zOReg(N + EBIT - 1 downto 0); 
                    end if;
                else
                    zN_next(N + EBIT - 1 downto 0) <= zOReg(N + EBIT - 1 downto 0) + tanIReg(N + EBIT - 1 downto 0); 
                    zN_next(N + EBIT) <= zOReg(N + EBIT);
                end if;
                stateNext <= donee;
            when donee =>
                done      <= '1';
                stateNext <= idle;
        end case;      
    end process; 
    
-- sign detection
    signOfY2 <= sigmaReg xor xOReg(N + EBIT);
    signOfX2 <= sigmaReg xor '1' xor yOReg(N + EBIT);
    tInverseSign <= sigmaReg xor '1' xor tanIReg(N + EBIT);      
    maxZo <= '1' when (zOReg(N + EBIT - 1 downto 0) > tanIReg(N + EBIT - 1 downto 0)) else '0';
    maxYo <= '1' when (yOReg(N + EBIT - 1 downto 0) > xShifted(N + EBIT - 1 downto 0)) else '0';
    maxXo <= '1' when (xOReg(N + EBIT - 1 downto 0) > yShifted(N + EBIT - 1 downto 0)) else '0';
-- formula condition
    x1x2XOR <= xOReg(N + EBIT) xor signOfX2;
    y1y2XOR <= yOReg(N + EBIT) xor signOfY2;
    z1z2XOR <= zOReg(N + EBIT) xor tInverseSign;
                 
---- x
 with i select xShifted(N + EBIT - 1 downto 0) <=
    xOReg(N + EBIT - 1 downto 0)                        when 0,
    '0' & xOReg(N + EBIT - 1 downto 1)                  when 1,
    "00" & xOReg(N + EBIT - 1 downto 2)                 when 2,
    "000" & xOReg(N + EBIT - 1 downto 3)                when 3,
    "0000" & xOReg(N + EBIT - 1 downto 4)               when 4,
    "00000" & xOReg(N + EBIT - 1 downto 5)              when 5,
    "000000" & xOReg(N + EBIT - 1 downto 6)             when 6,
    "0000000" & xOReg(N + EBIT - 1 downto 7)            when 7,
    "00000000" & xOReg(N + EBIT - 1 downto 8)           when 8,
    "000000000" & xOReg(N + EBIT - 1 downto 9)          when 9,
    "0000000000" & xOReg(N + EBIT - 1 downto 10)        when 10,
    "00000000000" & xOReg(N + EBIT - 1 downto 11)       when 11,
    "000000000000" & xOReg(N + EBIT - 1 downto 12)      when 12,
    "0000000000000" & xOReg(N + EBIT - 1 downto 13)     when 13,
    "00000000000000" & xOReg(N + EBIT - 1 downto 14)    when 14,
    "000000000000000" & xOReg(N + EBIT - 1 downto 15)   when 15,
    "0000000000000000" & xOReg(N + EBIT - 1 downto 16)  when 16,
    "00000000000000000" & xOReg(N + EBIT - 1 downto 17) when others;
    
-- y    
 with i select yShifted(N + EBIT - 1 downto 0) <=
      yOReg(N + EBIT - 1 downto 0)                        when 0,
      '0' & yOReg(N + EBIT - 1 downto 1)                  when 1,
      "00" & yOReg(N + EBIT - 1 downto 2)                 when 2,
      "000" & yOReg(N + EBIT - 1 downto 3)                when 3,
      "0000" & yOReg(N + EBIT - 1 downto 4)               when 4,
      "00000" & yOReg(N + EBIT - 1 downto 5)              when 5,
      "000000" & yOReg(N + EBIT - 1 downto 6)             when 6,
      "0000000" & yOReg(N + EBIT - 1 downto 7)            when 7,
      "00000000" & yOReg(N + EBIT - 1 downto 8)           when 8,
      "000000000" & yOReg(N + EBIT - 1 downto 9)          when 9,
      "0000000000" & yOReg(N + EBIT - 1 downto 10)        when 10,
      "00000000000" & yOReg(N + EBIT - 1 downto 11)       when 11,
      "000000000000" & yOReg(N + EBIT - 1 downto 12)      when 12,
      "0000000000000" & yOReg(N + EBIT - 1 downto 13)     when 13,
      "00000000000000" & yOReg(N + EBIT - 1 downto 14)    when 14,
      "000000000000000" & yOReg(N + EBIT - 1 downto 15)   when 15,
      "0000000000000000" & yOReg(N + EBIT - 1 downto 16)  when 16,
      "00000000000000000" & yOReg(N + EBIT - 1 downto 17) when others;
                 

-- Output
    sigmaNew <= zN_reg(N + EBIT);
    xNew     <= std_logic_vector(xN_reg);
    yNew     <= std_logic_vector(yN_reg);
    zNew     <= std_logic_vector(zN_reg);
    
end Behavioral;
