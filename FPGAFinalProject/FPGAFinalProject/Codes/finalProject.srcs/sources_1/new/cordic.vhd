library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

entity cordic is
    generic(n : integer := 18; 
            m: integer := 18;
            LogN : integer := 5);
    port(clk, reset: in std_logic;
         beta : in std_logic_vector(n downto 0);
         start : in std_logic;
         ready : out std_logic;
         result_ready : out std_logic;
         sinBeta : out std_logic_vector(n downto 0));
end cordic;

architecture Behavioral of cordic is
    component CordicComp is
        generic(
            N    : integer;
            EBIT : integer; --[log2(N)]
            I    : integer  --number of iteration
            );
        port(
            clk, reset       : in std_logic;
            start            : in std_logic;
            sigmaOld         : in std_logic;
            tanInverse      : in std_logic_vector(N + EBIT downto 0);
            zOld, xOld, yOld : in std_logic_vector(N + EBIT downto 0);
            done             : out std_logic;
            sigmaNew         : out std_logic;
            zNew, xNew, yNew : out std_logic_vector(N + EBIT downto 0)
            );
    end component;

    type mem2DConstants is array(0 to 17) of std_logic_vector(51 downto 0);
    signal gammaArr : mem2DConstants := (
        "0001100100100001111110110101010001000100001011010010",
        "0000111011010110001100111000001010110000110111011100",
        "0000011111010110110111010111111001001011001000000001",
        "0000001111111010101101110101001101010101100001011110",
        "0000000111111111010101011011101101110010110011111111",
        "0000000011111111111010101010110111011101010010111100",
        "0000000001111111111111010101010101101110111011011110",
        "0000000000111111111111111010101010101011011101110110",
        "0000000000011111111111111111010101010101010110111101",
        "0000000000001111111111111111111010101010101010101110",
        "0000000000000111111111111111111111010101010101010101",
        "0000000000000011111111111111111111111010101010100111",
        "0000000000000001111111111111111111111111010101010101",
        "0000000000000000111111111111111111111111111010101000",
        "0000000000000000011111111111111111111111111111010010",
        "0000000000000000001111111111111111111111111111111101",
        "0000000000000000000111111111111111111111111111111110",
        "0000000000000000000011111111111111111111111111111111");
        
    signal KArr : mem2DConstants := (
        "0001011010100000100111100110011001111111001110111110",
        "0001010000111101000100110110001001001000010010010011",
        "0001001110100010011000011011101001101101011110100101",
        "0001001101111011100100010100000111011110101100111110",
        "0001001101110001110110101100000110000010111011110000",
        "0001001101101111011011001111101010111101100101100100",
        "0001001101101110110100011000011010011111001010000000",
        "0001001101101110101010101010100101110000101100011111",
        "0001001101101110101000001111001000100010101001101011",
        "0001001101101110100111101000010001001110111111010100",
        "0001001101101110100111011110100011011010000100000010",
        "0001001101101110100111011100000111111100110101001100",
        "0001001101101110100111011011100001000101100001100010",
        "0001001101101110100111011011010111010111101100100101",
        "0001001101101110100111011011010100111100001111010101",
        "0001001101101110100111011011010100010101011000000011",
        "0001001101101110100111011011010100001011101010010000",
        "0001001101101110100111011011010100001001001110101111");

    type mem2DDatas is array(0 to m) of std_logic_vector(n+LogN downto 0);
    signal x, y, z : mem2DDatas := (others => (others =>'0'));
    signal sigma : std_logic_vector(m downto 0);
    signal componentsDone_next, componentsStart_next, componentsDone_reg, componentsStart_reg : std_logic_vector(m-1 downto 0);
    signal start_reg, ready_next, ready_reg: std_logic;
    
begin
    generateComponents: for i in 0 to m-1 generate
      CC: CordicComp generic map(N => n, EBIT => LogN, I => i)
             port map(
                clk => clk, 
                reset => reset, 
                start => componentsStart_reg(i),
                sigmaOld => sigma(i),
                tanInverse => gammaArr(i)(51 downto 51-(n+LogN)), 
                zOld => z(i), 
                xOld => x(i), 
                yOld => y(i), 
                done => componentsDone_next(i), 
                sigmaNew => sigma(i+1),
                zNew => z(i+1),
                xNew => x(i+1),
                yNew => y(i+1));
                
    end generate generateComponents;

    process(clk) 
    begin
        if(reset = '1') then
            componentsStart_reg <= (others => '0');
            ready_reg <= '1';
        elsif(clk = '1' and clk'event) then
            componentsDone_reg <= componentsStart_next;
            componentsStart_reg <= componentsStart_next;
            start_reg <= start;
            ready_reg <= ready_next;
        end if;
    end process;

    process(start, start_reg, componentsDone_reg, componentsStart_reg, componentsDone_next)
    begin
        ready_next <= ready_reg;
        componentsStart_next <= (others => '0');
        
        ready_next <= ready_reg;
        if(start = '1' and start_reg = '0') then
            z(0)(n+LogN downto LogN) <= beta;
            z(0)(LogN-1 downto 0) <= (others => '0');
            y(0) <= (others =>'0');
--            y(0)(n+LogN downto LogN) <= beta;
--            y(0)(LogN-1 downto 0) <= (others => '0');
            x(0) <= KArr(m-1)(51 downto 51-(n+LogN));
            sigma(0) <= beta(n);
            componentsStart_next(0) <= '1';
            ready_next <= '0';
        end if;
        
        if(componentsDone_reg(0) = '0' and componentsDone_next(0) = '1') then
            ready_next <= '1';
        end if;
        
        for i in 1 to m-1 loop
            if(componentsDone_reg(i-1) = '0' and componentsDone_next(i-1) = '1') then
                componentsStart_next(i) <= '1';
            end if;
        end loop;
        
    end process;
    
    result_ready <= componentsDone_next(m-1);
    sinBeta <= y(m)(n+LogN downto LogN);
    ready <= ready_reg;
    --sinBeta <= x(m)(n+LogN downto LogN);
end Behavioral;
