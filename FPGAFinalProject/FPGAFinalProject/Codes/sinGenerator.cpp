// C++ program to convert fractional decimal 
// to binary number 
#include<bits/stdc++.h> 
#include<math.h>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
using namespace std; 
  
// Function to convert decimal to binary upto 
// k-precision after decimal point 
string decimalToBinary(double num, int k_prec) 
{ 
    string binary = ""; 
  
    // Fetch the integral part of decimal number 
    int Integral = num; 
  
    // Fetch the fractional part decimal number 
    double fractional = num - Integral; 
  
    // Conversion of integral part to 
    // binary equivalent 
    while (Integral) 
    { 
        int rem = Integral % 2; 
  
        // Append 0 in binary 
        binary.push_back(rem +'0'); 
  
        Integral /= 2; 
    } 
  
    // Reverse string to get original binary 
    // equivalent 
    reverse(binary.begin(),binary.end()); 
  
    // Append point before conversion of 
    // fractional part 
    binary.push_back('.'); 
  
    // Conversion of fractional part to 
    // binary equivalent 
    while (k_prec--) 
    { 
        // Find next bit in fraction 
        fractional *= 2; 
        int fract_bit = fractional; 
  
        if (fract_bit == 1) 
        { 
            fractional -= fract_bit; 
            binary.push_back(1 + '0'); 
        } 
        else
            binary.push_back(0 + '0'); 
    } 
  
    return binary; 
} 
int main() {
    srand (time(NULL));
    for(int i = 0; i < 20; i++) {
        double r = (((double) rand() / (RAND_MAX))*180)-90;
        double num = r*acos(-1)/180;
        cout << "num: " << num << " ==> ";
        if(num < 0)
            cout << "-";
        cout << decimalToBinary(abs(num), 20) << endl << "sin(num): " << sin(num) << " ==> ";
        if(num < 0)
            cout << "-";
        cout << decimalToBinary(abs(sin(num)), 20) << "\n\n";
    }
    

    return 0;
}

