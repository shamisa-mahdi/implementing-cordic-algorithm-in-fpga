--library IEEE;
--use IEEE.Std_logic_1164.all;
--use IEEE.Numeric_Std.all;

--entity fullAdderSubtractor_tb is
--end;

--architecture bench of fullAdderSubtractor_tb is

--  component fullAdderSubtractor
--    port(
--      a   : in std_logic;
--      b   : in std_logic;
--      cin : in std_logic;
--      sub : in std_logic;
--      cout: out std_logic;
--      o   : out std_logic
--      );
--  end component;

--  signal a: std_logic;
--  signal b: std_logic;
--  signal cin: std_logic;
--  signal sub: std_logic;
--  signal cout: std_logic;
--  signal o: std_logic ;

--begin

--  uut: fullAdderSubtractor port map ( a    => a,
--                                      b    => b,
--                                      cin  => cin,
--                                      sub  => sub,
--                                      cout => cout,
--                                      o    => o );

--  stimulus: process
--  begin
  
--       a <= '1';
--       b <= '0';
--       cin <= '0';
--       sub <= '0';
--       wait for 10 ns;

--       a <= '1';
--       b <= '1';
--       cin <= '0';
--       sub <= '0';
--       wait for 10 ns;
       
--       a <= '1';
--       b <= '1';
--       cin <= '1';
--       sub <= '0';
--       wait for 10 ns;
   
--       a <= '1';
--       b <= '1';
--       cin <= '0';
--       sub <= '1';
--       wait for 10 ns;
     
--       a <= '1';
--       b <= '0';
--       cin <= '1';
--       sub <= '1';
--       wait for 10 ns;
       
--       a <= '0';
--       b <= '1';
--       cin <= '0';
--       sub <= '1';
--       wait for 10 ns;
--    wait;
--  end process;


--end;