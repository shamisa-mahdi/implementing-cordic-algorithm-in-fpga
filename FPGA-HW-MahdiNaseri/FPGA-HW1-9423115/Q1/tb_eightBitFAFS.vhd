library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity eightBitAdderSubtractor_tb is
end;

architecture bench of eightBitAdderSubtractor_tb is

  component eightBitAdderSubtractor
    port(
      A : in  STD_LOGIC_VECTOR (7 downto 0);
      B : in  STD_LOGIC_VECTOR (7 downto 0);
      Cin : in  STD_LOGIC;
      Sub : in STD_LOGIC;
      O : out  STD_LOGIC_VECTOR (7 downto 0);
      Cout : out  STD_LOGIC
    );
  end component;

  signal A: STD_LOGIC_VECTOR (7 downto 0);
  signal B: STD_LOGIC_VECTOR (7 downto 0);
  signal Cin: STD_LOGIC;
  signal Sub: STD_LOGIC;
  signal O: STD_LOGIC_VECTOR (7 downto 0);
  signal Cout: STD_LOGIC ;

begin

  uut: eightBitAdderSubtractor port map ( A    => A,
                                          B    => B,
                                          Cin  => Cin,
                                          Sub  => Sub,
                                          O    => O,
                                          Cout => Cout );

  stimulus: process
  begin
  
    -- Put initialisation code here
    Cin <= '1';
    Sub <= '0';
    A <= "01001010";
    B <= "01000011";
    wait for 10 ns;

    Cin <= '0';
    Sub <= '1';
    A <= "10011010";
    B <= "00010011";
    wait for 10 ns;
    
    Cin <= '0';
    Sub <= '0';
    A <= "00001010";
    B <= "10110011";
    wait for 10 ns;
    
    -- Put test bench stimulus code here

    wait;
  end process;


end;