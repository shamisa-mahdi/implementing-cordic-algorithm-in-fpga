library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity fourBitMultiplier_tb is
end;

architecture bench of fourBitMultiplier_tb is

  component fourBitMultiplier
      Port (x : in STD_LOGIC_VECTOR(3 downto 0);
            y : in STD_LOGIC_VECTOR(3 downto 0);
            o : out STD_LOGIC_VECTOR(7 downto 0));
  end component;

  signal x: STD_LOGIC_VECTOR(3 downto 0);
  signal y: STD_LOGIC_VECTOR(3 downto 0);
  signal o: STD_LOGIC_VECTOR(7 downto 0);

begin

  uut: fourBitMultiplier port map ( x => x,
                                    y => y,
                                    o => o );

  stimulus: process
  begin
  
    -- Put initialisation code here

--    x <= "0100";
--    y <= "0101";
--    wait for 10 ms;
    
--    x <= "0110";
--    y <= "1100";
--    wait for 10 ms;
    
    x <= "1111";
    y <= "1111";
    wait for 10 ms;
    -- Put test bench stimulus code here
    
    wait;
  end process;


end;