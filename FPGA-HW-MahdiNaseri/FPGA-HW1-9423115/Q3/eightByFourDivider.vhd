library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity eightByFourDivider is
    Port (a : in STD_LOGIC_VECTOR(7 downto 0);
          b : in STD_LOGIC_VECTOR(3 downto 0);
          q : out STD_LOGIC_VECTOR(7 downto 0);
          r : out STD_LOGIC_VECTOR(3 downto 0));
end eightByFourDivider;

architecture Behavioral of eightByFourDivider is
    component eightBitAdderSubtractor
      port(
        A : in  STD_LOGIC_VECTOR (7 downto 0);
        B : in  STD_LOGIC_VECTOR (7 downto 0);
        Cin : in  STD_LOGIC;
        Sub : in STD_LOGIC;
        O : out  STD_LOGIC_VECTOR (7 downto 0);
        Cout : out  STD_LOGIC
      );
    end component;
    
    signal D0: STD_LOGIC_VECTOR(7 downto 0);
    signal D1: STD_LOGIC_VECTOR(7 downto 0);
    signal D2: STD_LOGIC_VECTOR(7 downto 0);
    signal D3: STD_LOGIC_VECTOR(7 downto 0);
    signal D4: STD_LOGIC_VECTOR(7 downto 0);
    signal D5: STD_LOGIC_VECTOR(7 downto 0);
    signal D6: STD_LOGIC_VECTOR(7 downto 0);
    signal D7: STD_LOGIC_VECTOR(7 downto 0);
    
    signal A0: STD_LOGIC_VECTOR(7 downto 0);
    signal A1: STD_LOGIC_VECTOR(7 downto 0);
    signal A2: STD_LOGIC_VECTOR(7 downto 0);
    signal A3: STD_LOGIC_VECTOR(7 downto 0);
    signal A4: STD_LOGIC_VECTOR(7 downto 0);
    signal A5: STD_LOGIC_VECTOR(7 downto 0);
    signal A6: STD_LOGIC_VECTOR(7 downto 0);
    signal A7: STD_LOGIC_VECTOR(7 downto 0);
    
    signal B0: STD_LOGIC_VECTOR(7 downto 0);
    signal B1: STD_LOGIC_VECTOR(7 downto 0);
    signal B2: STD_LOGIC_VECTOR(7 downto 0);
    signal B3: STD_LOGIC_VECTOR(7 downto 0);
    signal B4: STD_LOGIC_VECTOR(7 downto 0);
    signal B5: STD_LOGIC_VECTOR(7 downto 0);
    signal B6: STD_LOGIC_VECTOR(7 downto 0);
    signal B7: STD_LOGIC_VECTOR(7 downto 0);
    
    signal tempR: STD_LOGIC_VECTOR(7 downto 0);
    signal cout: STD_LOGIC_VECTOR(7 downto 0);
begin
    A0 <= a;
    A1 <= D0 when (cout(0) = '0' and (b(3 downto 1) = "000")) else A0;
    A2 <= D1 when (cout(1) = '0' and (b(3 downto 2) = "00")) else A1;
    A3 <= D2 when (cout(2) = '0' and (b(3) = '0')) else A2;
    A4 <= D3 when cout(3) = '0' else A3;
    A5 <= D4 when cout(4) = '0' else A4;
    A6 <= D5 when cout(5) = '0' else A5;
    A7 <= D6 when cout(6) = '0' else A6;
    tempR <= D7 when cout(7) = '0' else A7;
    r <= tempR(3 downto 0);
    
    q(7) <= '1' when (cout(0) = '0' and (b(3 downto 1) = "000")) else '0';
    q(6) <= '1' when (cout(1) = '0' and (b(3 downto 2) = "00")) else '0';
    q(5) <= '1' when (cout(2) = '0' and (b(3) = '0')) else '0';
    q(4) <= '1' when (cout(3) = '0') else '0';
    q(3) <= '1' when (cout(4) = '0') else '0';
    q(2) <= '1' when (cout(5) = '0') else '0';
    q(1) <= '1' when (cout(6) = '0') else '0';
    q(0) <= '1' when (cout(7) = '0') else '0';
    
    B0 <= b(0) & "0000000";
    B1 <= b(1 downto 0) & "000000";
    B2 <= b(2 downto 0) & "00000";
    B3 <= b(3 downto 0) & "0000";
    B4 <= '0' & b(3 downto 0) & "000";
    B5 <= "00" & b(3 downto 0) & "00";
    B6 <= "000" & b(3 downto 0) & "0";
    B7 <= "0000" & b(3 downto 0);
    
    C0: eightBitAdderSubtractor port map(A => A0, B => B0, Cin => '0', Sub => '1', O => D0, Cout => cout(0));
    C1: eightBitAdderSubtractor port map(A => A1, B => B1, Cin => '0', Sub => '1', O => D1, Cout => cout(1));
    C2: eightBitAdderSubtractor port map(A => A2, B => B2, Cin => '0', Sub => '1', O => D2, Cout => cout(2));
    C3: eightBitAdderSubtractor port map(A => A3, B => B3, Cin => '0', Sub => '1', O => D3, Cout => cout(3));
    C4: eightBitAdderSubtractor port map(A => A4, B => B4, Cin => '0', Sub => '1', O => D4, Cout => cout(4));
    C5: eightBitAdderSubtractor port map(A => A5, B => B5, Cin => '0', Sub => '1', O => D5, Cout => cout(5));
    C6: eightBitAdderSubtractor port map(A => A6, B => B6, Cin => '0', Sub => '1', O => D6, Cout => cout(6));
    C7: eightBitAdderSubtractor port map(A => A7, B => B7, Cin => '0', Sub => '1', O => D7, Cout => cout(7));

end Behavioral;
