library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity fullAdderSubtractor is
  port(
    a   : in std_logic;
    b   : in std_logic;
    cin : in std_logic;
    sub : in std_logic;
    cout: out std_logic;
    o   : out std_logic
    );
end fullAdderSubtractor;
 
architecture rtl of fullAdderSubtractor is   
begin
  o <= (a xor b) xor cin;
  cout <= (cin and not(a xor b)) or (not(a) and b) when sub = '1' else
          (cin and (a xor b)) or (a and b);
end rtl;