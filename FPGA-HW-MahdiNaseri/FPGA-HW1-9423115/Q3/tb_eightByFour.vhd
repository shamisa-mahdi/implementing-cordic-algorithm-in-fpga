library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity eightByFourDivider_tb is
end;

architecture bench of eightByFourDivider_tb is

  component eightByFourDivider
      Port (a : in STD_LOGIC_VECTOR(7 downto 0);
            b : in STD_LOGIC_VECTOR(3 downto 0);
            q : out STD_LOGIC_VECTOR(7 downto 0);
            r : out STD_LOGIC_VECTOR(3 downto 0));
  end component;

  signal a: STD_LOGIC_VECTOR(7 downto 0);
  signal b: STD_LOGIC_VECTOR(3 downto 0);
  signal q: STD_LOGIC_VECTOR(7 downto 0);
  signal r: STD_LOGIC_VECTOR(3 downto 0);

begin

  uut: eightByFourDivider port map ( a => a,
                                     b => b,
                                     q => q,
                                     r => r );

  stimulus: process
  begin
  
    a <= "10101010";
    b <= "0110";
    wait for 10 ns;

    wait;
  end process;


end;
  