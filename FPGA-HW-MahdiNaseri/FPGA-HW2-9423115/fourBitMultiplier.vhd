library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fourBitMultiplier is
    Port (x : in STD_LOGIC_VECTOR(3 downto 0);
          y : in STD_LOGIC_VECTOR(3 downto 0);
          o : out STD_LOGIC_VECTOR(7 downto 0));
end fourBitMultiplier;

architecture Behavioral of fourBitMultiplier is
    component eightBitAdderSubtractor is
      port(
          A : in  STD_LOGIC_VECTOR(7 downto 0);
          B : in  STD_LOGIC_VECTOR(7 downto 0);
          Cin : in  STD_LOGIC;
          Sub : in STD_LOGIC;
          O : out  STD_LOGIC_VECTOR (7 downto 0);
          Cout : out  STD_LOGIC
        );
    end component;
    
    signal P0: STD_LOGIC_VECTOR(7 downto 0);
    signal P1: STD_LOGIC_VECTOR(7 downto 0);
    signal P2: STD_LOGIC_VECTOR(7 downto 0);
    signal P3: STD_LOGIC_VECTOR(7 downto 0);
    signal tempSum0: STD_LOGIC_VECTOR(7 downto 0);
    signal tempSum1: STD_LOGIC_VECTOR(7 downto 0);
begin
    P0 <= "0000" & (y(0) and x(3)) & (y(0) and x(2)) & (y(0) and x(1)) & (y(0) and x(0));
    P1 <= "000" & (y(1) and x(3)) & (y(1) and x(2)) & (y(1) and x(1)) & (y(1) and x(0)) & '0';
    P2 <= "00" & (y(2) and x(3)) & (y(2) and x(2)) & (y(2) and x(1)) & (y(2) and x(0)) & "00";
    P3 <= '0' & (y(3) and x(3)) & (y(3) and x(2)) & (y(3) and x(1)) & (y(3) and x(0)) & "000";
    
    C0: eightBitAdderSubtractor port map (A => P0, B => P1, Cin => '0', Sub => '0', O => tempSum0);
    C1: eightBitAdderSubtractor port map (A => tempSum0, B => P2, Cin => '0', Sub => '0', O => tempSum1);
    C2: eightBitAdderSubtractor port map (A => tempSum1, B => P3, Cin => '0', Sub => '0', O => o);    

end Behavioral;
