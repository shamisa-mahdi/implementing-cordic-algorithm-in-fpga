library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity fixedPointMultiplier_tb is
end;

architecture bench of fixedPointMultiplier_tb is

  component fixedPointMultiplier
      port(
          a : in STD_LOGIC_VECTOR(4 downto 0);
          pa: in STD_LOGIC_VECTOR(1 downto 0);
          b : in STD_LOGIC_VECTOR(4 downto 0);
          pb: in STD_LOGIC_VECTOR(1 downto 0);
          c : out STD_LOGIC_VECTOR(8 downto 0);
          pc: out STD_LOGIC_VECTOR(2 downto 0)
          );
  end component;

  signal a: STD_LOGIC_VECTOR(4 downto 0);
  signal pa: STD_LOGIC_VECTOR(1 downto 0);
  signal b: STD_LOGIC_VECTOR(4 downto 0);
  signal pb: STD_LOGIC_VECTOR(1 downto 0);
  signal c: STD_LOGIC_VECTOR(8 downto 0);
  signal pc: STD_LOGIC_VECTOR(2 downto 0) ;

begin

  uut: fixedPointMultiplier port map ( a  => a,
                                       pa => pa,
                                       b  => b,
                                       pb => pb,
                                       c  => c,
                                       pc => pc );

  stimulus: process
  begin
 
    a <= "01011";
    b <= "01100";
    pa <= "10";
    pb <= "01";
    wait for 100 ns;
    
    a <= "01011";
    b <= "01100";
    pa <= "11";
    pb <= "11";
    wait for 100 ns;
    
    a <= "10111";
    b <= "00010";
    pa <= "00";
    pb <= "10";
    wait for 100 ns;
    
    a <= "10101";
    b <= "10001";
    pa <= "11";
    pb <= "01";
    wait for 100 ns;
  end process;


end;