library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fixedPointMultiplier is
    port(
        a : in STD_LOGIC_VECTOR(4 downto 0);
        pa: in STD_LOGIC_VECTOR(1 downto 0);
        b : in STD_LOGIC_VECTOR(4 downto 0);
        pb: in STD_LOGIC_VECTOR(1 downto 0);
        c : out STD_LOGIC_VECTOR(8 downto 0);
        pc: out STD_LOGIC_VECTOR(2 downto 0)
        );
end fixedPointMultiplier;

architecture Behavioral of fixedPointMultiplier is
    component fourBitMultiplier is
        port(
              x : in STD_LOGIC_VECTOR(3 downto 0);
              y : in STD_LOGIC_VECTOR(3 downto 0);
              o : out STD_LOGIC_VECTOR(7 downto 0)
              );
    end component;
    
    component eightBitAdderSubtractor is
      port(
            A : in  STD_LOGIC_VECTOR(7 downto 0);
            B : in  STD_LOGIC_VECTOR(7 downto 0);
            Cin : in  STD_LOGIC;
            Sub : in STD_LOGIC;
            O : out  STD_LOGIC_VECTOR(7 downto 0);
            Cout : out  STD_LOGIC
          );
    end component;
    
    signal tempPa : STD_LOGIC_VECTOR(7 downto 0);
    signal tempPb : STD_LOGIC_VECTOR(7 downto 0);
    signal tempPc : STD_LOGIC_VECTOR(7 downto 0);
    signal tempa : STD_LOGIC_VECTOR(3 downto 0);
    signal tempb : STD_LOGIC_VECTOR(3 downto 0);
    signal tempc : STD_LOGIC_VECTOR(7 downto 0);
begin
    tempPa <= "000000" & pa;
    tempPb <= "000000" & pb;
    tempa <= a(3 downto 0);
    tempb <= b(3 downto 0);
    
    S0: eightBitAdderSubtractor port map(A => tempPa, B => tempPb, Cin => '0', Sub => '0', O => tempPc);
    pc <= tempPc(2 downto 0);
    
    P0: fourBitMultiplier port map(x => tempa, y => tempb, o => tempc);
    c(7 downto 0) <= tempc;
    
    c(8) <= a(4) xor b(4);
    
end Behavioral;
