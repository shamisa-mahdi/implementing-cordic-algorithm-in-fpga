library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity eightBitAdderSubtractor is
  port(
    A : in  STD_LOGIC_VECTOR (7 downto 0);
    B : in  STD_LOGIC_VECTOR (7 downto 0);
    Cin : in  STD_LOGIC;
    Sub : in STD_LOGIC;
    O : out  STD_LOGIC_VECTOR (7 downto 0);
    Cout : out  STD_LOGIC
  );
end eightBitAdderSubtractor;

architecture rtl of eightBitAdderSubtractor is
    component fullAdderSubtractor
      port(
        a   : in std_logic;
        b   : in std_logic;
        cin : in std_logic;
        sub : in std_logic;
        cout: out std_logic;
        o   : out std_logic
        );
    end component;
   signal c: STD_LOGIC_VECTOR(6 downto 0);
begin

  C0: fullAdderSubtractor port map (a => A(0), b => B(0), cin => Cin, o => O(0), cout => c(0), sub => Sub);
  C1: fullAdderSubtractor port map (a => A(1), b => B(1), cin => c(0), o => O(1), cout => c(1), sub => Sub);
  C2: fullAdderSubtractor port map (a => A(2), b => B(2), cin => c(1), o => O(2), cout => c(2), sub => Sub);
  C3: fullAdderSubtractor port map (a => A(3), b => B(3), cin => c(2), o => O(3), cout => c(3), sub => Sub);
  C4: fullAdderSubtractor port map (a => A(4), b => B(4), cin => c(3), o => O(4), cout => c(4), sub => Sub);
  C5: fullAdderSubtractor port map (a => A(5), b => B(5), cin => c(4), o => O(5), cout => c(5), sub => Sub);
  C6: fullAdderSubtractor port map (a => A(6), b => B(6), cin => c(5), o => O(6), cout => c(6), sub => Sub);
  C7: fullAdderSubtractor port map (a => A(7), b => B(7), cin => c(6), o => O(7), cout => Cout, sub => Sub);

end rtl;