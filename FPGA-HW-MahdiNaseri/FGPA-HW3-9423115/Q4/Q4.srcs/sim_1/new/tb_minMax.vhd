library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity minMax_tb is
end;

architecture bench of minMax_tb is

  component minMax
    port (
      clk   : in std_logic;
      reset : in std_logic;
      start : in std_logic;
      data : in std_logic_vector(7 downto 0);
      output  : out std_logic_vector(7 downto 0);
      done : out std_logic
    );
  end component;
  signal clk: std_logic;
  signal reset: std_logic;
  signal start: std_logic;
  signal data: std_logic_vector(7 downto 0);
  signal output: std_logic_vector(7 downto 0);
  signal done: std_logic ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: minMax port map ( 
                         clk           => clk,
                         reset         => reset,
                         start         => start,
                         data          => data,
                         output        => output,
                         done          => done );

  stimulus: process
  begin
    reset <= '1';
    start <= '0';
    data <= "00000000";
    wait for clock_period;
    reset <= '0';
    wait for clock_period;
    start <= '1';
    wait for clock_period;
    start <= '0';
    
    data <= "00000001";
    wait for clock_period;
    data <= "00000010";
    wait for clock_period;
    data <= "00100000";
    wait for clock_period;
    data <= "00000100";
    wait for clock_period;
    data <= "00000010";
    wait for clock_period;
    data <= "00000100";
    wait for clock_period;
    data <= "10000001";
    wait for clock_period;
    data <= "00100000";
    wait for clock_period;
    data <= "00010000";
    wait for clock_period;
    data <= "00001000";
    wait for clock_period * 5;
    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;