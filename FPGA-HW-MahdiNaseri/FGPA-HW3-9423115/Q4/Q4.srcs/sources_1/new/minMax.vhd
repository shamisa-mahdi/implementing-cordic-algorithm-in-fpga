library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity minMax is
  port (
--         minO : out std_logic_vector(7 downto 0);
--         maxO : out std_logic_vector(7 downto 0);
--         counter_regO : out std_logic_vector(3 downto 0);
--         counter_nextO : out std_logic_vector(3 downto 0);
--         stateO : out std_logic_vector(1 downto 0);
    clk   : in std_logic;
    reset : in std_logic;
    start : in std_logic;
    data : in std_logic_vector(7 downto 0);
    output  : out std_logic_vector(7 downto 0);
    done : out std_logic
  );
end minMax;

architecture arch of minMax is
    signal min : unsigned(7 downto 0);
    signal max : unsigned(7 downto 0);
    signal counter_reg : unsigned(3 downto 0);
    signal counter_next : unsigned(3 downto 0);
    signal state : unsigned(1 downto 0);
    signal avg : unsigned(7 downto 0);
begin
    process(reset, clk, start)
    begin
        if(reset = '1') then
            max <= "00000000";
            min <= "11111111";
            counter_reg <= "0000";
            state <= "00";
            done <= '0';
            avg <= "00000000";
        elsif(start = '1' and state = "00") then
            state <= "01";
        elsif(start = '0' and state = "01") then
            state <= "10";
        elsif(clk'event and clk = '1' and state = "10") then
            counter_reg <= counter_next + 1;
            if(counter_reg < "1011") then
                if(unsigned(data) > max) then
                    max <= unsigned(data);
                end if;
                if(unsigned(data) < min) then
                    min <= unsigned(data);
                end if;
           elsif(counter_reg = "1011") then
                counter_reg <= "0000";
                avg <= (min + max)/2;
                done <= '1';
                state <= "11";
            end if;
        elsif(clk'event and clk = '1' and state = "11") then
            done <= '0';
            state <= "00";
        end if;
    end process;
    counter_next <= counter_reg;   
    output <= std_logic_vector(avg);
--    minO <= std_logic_vector(min);
--    maxO <= std_logic_vector(max);
--    counter_regO <= std_logic_vector(counter_reg);
--    counter_nextO <= std_logic_vector(counter_next);
--    stateO <= std_logic_vector(state);
end arch;