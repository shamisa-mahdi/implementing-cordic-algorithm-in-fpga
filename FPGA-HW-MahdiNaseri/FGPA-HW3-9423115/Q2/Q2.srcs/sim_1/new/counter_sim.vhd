library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity counter_tb is
end;

architecture bench of counter_tb is

  component counter
      generic (
          N : integer := 6;
          M : integer := 3
      );
      port(
          clk : in std_logic;
          reset : in std_logic;
          clk_out : out std_logic
      );
  end component;

  signal clk: std_logic;
  signal reset: std_logic;
  signal clk_out: std_logic ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: counter generic map ( N       => 6,
                             M       => 3)
                  port map ( clk     => clk,
                             reset   => reset,
                             clk_out => clk_out );

  stimulus: process
  begin
   reset <= '1';
   wait for clock_period/2;
   reset <= '0';
    wait for 36 * clock_period;
    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;