library ieee ;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity counter is
    generic (
        N : integer := 6;
        M : integer := 3 -- number of bits for N
    );
    port(
        clk : in std_logic;
        reset : in std_logic;
        clk_out : out std_logic
    );
end counter;

architecture arch of counter is
    signal r_reg : unsigned(M downto 0);
    signal r_next : unsigned(M downto 0);
    signal clk_sig : std_logic;
begin
    process(clk , reset)
        begin
        if(clk'event) then
            if(reset = '1') then
                clk_sig <= '0';
                r_reg <= (others => '0');
            else
                r_reg <= r_next;
                if(r_reg = N-1) then
                    clk_sig <= not clk_sig;
                end if;
            end if;
        end if ;
    end process;
    r_next <= (others => '0') when r_reg = N-1 else r_reg + 1;
    clk_out <= clk_sig;
end arch ;