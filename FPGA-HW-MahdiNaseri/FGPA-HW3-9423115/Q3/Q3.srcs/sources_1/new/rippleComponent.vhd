library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity rippleCounter is
  generic (
    N : integer := 8
  );
  port (
    clk   : in std_logic;
    reset : in std_logic;
    counter  : out std_logic_vector(N-1 downto 0)
  );
end rippleCounter;

architecture arch of rippleCounter is
  signal internalClk : std_logic_vector(N downto 0);
begin
  genCLK: for i in 0 to N-1 generate
    process(reset, internalClk)
    begin
      if(reset = '1') then
        internalClk(i+1) <= '0';
      elsif(internalClk(i)'event and internalClk(i) = '0') then
        internalClk(i+1) <= not internalClk(i+1);
      end if;
    end process dff;
  end generate;
  
  internalClk(0) <= clk;
  counter <= internalClk(N downto 1);
end arch;