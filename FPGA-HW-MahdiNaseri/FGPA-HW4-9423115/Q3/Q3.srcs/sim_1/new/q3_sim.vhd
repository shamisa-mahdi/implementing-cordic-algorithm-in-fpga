library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity eq3_tb is
end;

architecture bench of eq3_tb is

  component eq3
    port (
      clk   : in std_logic;
      reset : in std_logic;
      counterA : out std_logic_vector(8 downto 0);
      counterB : out std_logic_vector(8 downto 0);
      counterC : out std_logic_vector(8 downto 0);
      counterD : out std_logic_vector(8 downto 0);
      counterO : out std_logic_vector(7 downto 0);
      dataO: out std_logic_vector(7 downto 0)
    );
  end component;

  signal clk: std_logic;
  signal reset: std_logic;
  signal counterA: std_logic_vector(8 downto 0);
  signal counterB: std_logic_vector(8 downto 0);
  signal counterC: std_logic_vector(8 downto 0);
  signal counterD: std_logic_vector(8 downto 0);
  signal counterO: std_logic_vector(7 downto 0);
  signal dataO: std_logic_vector(7 downto 0) ;

  constant clock_period: time := 1 ns;
  signal stop_the_clock: boolean;

begin

  uut: eq3 port map ( clk      => clk,
                      reset    => reset,
                      counterA => counterA,
                      counterB => counterB,
                      counterC => counterC,
                      counterD => counterD,
                      counterO => counterO,
                      dataO    => dataO );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset <= '1';
    wait for 2*clock_period;
    reset <= '0';
    wait for 300 * clock_period;

    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
