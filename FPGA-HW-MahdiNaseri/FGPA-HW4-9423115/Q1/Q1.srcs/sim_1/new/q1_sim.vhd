library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity eq1_tb is
end;

architecture bench of eq1_tb is

  component eq1
    port (
      clk   : in std_logic;
      reset : in std_logic;
      a, b : in std_logic;
      f : out std_logic;
      size: out std_logic_vector(3 downto 0)
    );
  end component;

  signal clk: std_logic;
  signal reset: std_logic;
  signal a, b: std_logic;
  signal f: std_logic;
  signal size: std_logic_vector(3 downto 0) ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: eq1 port map ( clk   => clk,
                      reset => reset,
                      a     => a,
                      b     => b,
                      f     => f,
                      size  => size );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset <= '1';
    wait for 2*clock_period;
    reset <= '0';

    -- Put test bench stimulus code here
    a <= '0';
    b <= '0';
    wait for clock_period;
    a <= '1';
    wait for clock_period;
    
    a <= '0';
    wait for clock_period;
    a <= '1';
    wait for clock_period;
    
    a <= '0';
    wait for clock_period;
    a <= '1';
    wait for clock_period;
    
    a <= '0';
    wait for clock_period;
    a <= '1';
    wait for clock_period;
    
    a <= '0';
    wait for clock_period;
    a <= '1';
    wait for clock_period;
    
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    
    a <= '0';
    wait for clock_period;
    a <= '1';
    wait for clock_period;
    
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    b <= '0';
    wait for clock_period;
    b <= '1';
    wait for clock_period;
    
    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;