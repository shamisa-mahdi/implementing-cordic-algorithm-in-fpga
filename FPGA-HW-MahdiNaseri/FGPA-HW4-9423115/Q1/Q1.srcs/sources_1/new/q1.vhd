library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity eq1 is
  port (
    clk   : in std_logic;
    reset : in std_logic;
    a, b : in std_logic;
    f : out std_logic;
    size: out std_logic_vector(3 downto 0)
  );
end eq1;

architecture arch of eq1 is
    signal size_reg, size_next : unsigned(3 downto 0);
    signal a_reg, b_reg : std_logic;
    signal f_alias : std_logic;
begin
    process(reset, clk)
    begin
        if(reset = '1') then
            a_reg <= '0';
            b_reg <= '0';
            size_next <= "0101";
        elsif(clk'event and clk = '1') then
            --size_next <= size_reg;
            if(a_reg = '0' and a = '1' and not(b_reg = '0' and b = '1')) then
                if(f_alias = '0') then
                    size_next <= size_reg + 1;
                end if;
            end if;
            if(not(a_reg = '0' and a = '1') and (b_reg = '0' and b = '1')) then
                if(size_reg > 0) then
                    size_next <= size_reg - 1;
                end if;
            end if;
            b_reg <= b;
            a_reg <= a;
            size_reg <= size_next;
        end if;
    end process;

    f_alias <= '1' when size_reg = "1010" else '0';
    f <= f_alias;
    
    size <= std_logic_vector(size_reg);
end arch;