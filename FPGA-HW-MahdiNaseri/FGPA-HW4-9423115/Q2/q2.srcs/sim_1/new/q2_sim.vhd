library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity eq2_tb is
end;

architecture bench of eq2_tb is

  component eq2
    port (
      clk   : in std_logic;
      reset : in std_logic;
      input : in std_logic;
      output : out std_logic_vector(1 downto 0);
      counter_regO, counter_nextO : out std_logic_vector(3 downto 0);
      num_regO, num_nextO : out std_logic_vector(7 downto 0)
    );
  end component;

  signal clk: std_logic;
  signal reset: std_logic;
  signal input: std_logic;
  signal output: std_logic_vector(1 downto 0);
  signal counter_regO, counter_nextO: std_logic_vector(3 downto 0);
  signal num_regO, num_nextO: std_logic_vector(7 downto 0) ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: eq2 port map ( clk           => clk,
                      reset         => reset,
                      input         => input,
                      output        => output,
                      counter_regO  => counter_regO,
                      counter_nextO => counter_nextO,
                      num_regO      => num_regO,
                      num_nextO     => num_nextO );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset <= '1';
    wait for clock_period;
    reset <= '0';
    
    input <= '0';
    wait for clock_period * 2;
    
    --- FIRST NUMBER ---
    input <= '1';
    wait for clock_period;
    
    --
    input <= '1';
    wait for clock_period;
    
    input <= '1';
    wait for clock_period;
    
    input <= '0';
    wait for clock_period;
    
    input <= '0';
    wait for clock_period;
                    
    input <= '1';
    wait for clock_period;
    
    input <= '1';
    wait for clock_period;
    
    input <= '0';
    wait for clock_period;
    
    input <= '1';
    wait for clock_period;
    --
    input <= '0';
    wait for clock_period * 2;
   
   --- SECOND NUMBER ---
       input <= '1';
   wait for clock_period;
   
   --
   input <= '1';
   wait for clock_period;
   
   input <= '1';
   wait for clock_period;
   
   input <= '0';
   wait for clock_period;
   
   input <= '0';
   wait for clock_period;
                   
   input <= '1';
   wait for clock_period;
   
   input <= '1';
   wait for clock_period;
   
   input <= '1';
   wait for clock_period;
   
   input <= '0';
   wait for clock_period;
   --
   input <= '0';
   wait for clock_period * 2;
    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;