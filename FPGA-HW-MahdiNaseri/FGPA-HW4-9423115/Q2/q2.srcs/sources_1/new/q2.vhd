library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity eq2 is
  port (
    clk   : in std_logic;
    reset : in std_logic;
    input : in std_logic;
    output : out std_logic_vector(1 downto 0);
    counter_regO, counter_nextO : out std_logic_vector(3 downto 0);
    num_regO, num_nextO : out std_logic_vector(7 downto 0)
  );
end eq2;

architecture arch of eq2 is
    type state_type is (idle, start, data, stop);
    signal state_reg, state_next : state_type;
    signal counter_reg, counter_next : unsigned(3 downto 0);
    signal num_reg, num_next : unsigned(7 downto 0);
begin
    process(reset, clk)
    begin
        if(reset = '1') then
            state_reg <= idle;
            counter_reg <= (others => '0');
            num_reg <= (others => '0');
        elsif(clk'event and clk = '1') then
            state_reg <= state_next;
            counter_reg <= counter_next;
            num_reg <= num_next;
        end if;
    end process;
    
    process(input, state_reg, counter_reg)
    begin
        num_next <= num_reg;
        state_next <= state_reg;
        counter_next <= counter_reg;
        if(input = '1' and state_reg = idle) then
            state_next <= start;
            counter_next <= (others => '0');
        end if;
        
        if(state_reg = start) then
            num_next <= num_reg(6 downto 0) & input;
            state_next <= data;
        end if;
        
        if(state_reg = data) then
            if(counter_reg < "0111") then
                num_next <= num_reg(6 downto 0) & input;
                counter_next <= counter_reg + 1;
            else 
                state_next <= stop;
            end if;
        end if;
        
        if(state_reg = stop) then
            state_next <= idle;
        end if;
    end process;
    
    counter_regO <= std_logic_vector(counter_reg);
    counter_nextO <= std_logic_vector(counter_next);
    num_regO <= std_logic_vector(num_reg);
    num_nextO <= std_logic_vector(num_next);
    output <= std_logic_vector(num_reg mod "11");
end arch;